export class Todos {
    _id! :string;
    title! : string;
    description! : string;
    createdAt!:Date;
    updatedAt!:Date;
}