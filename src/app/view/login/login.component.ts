import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { Token } from '../../models/login/token.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  constructor(private router : Router,private loginService:LoginService){}

  username: string = '';
  password: string = '';

  login() {
    this.loginService.apiLogin(this.username,this.password).subscribe((res:Token)=>{
      if(res.token){
        localStorage.setItem('loginToken',res.token);
        this.router.navigate(['main']);
      }
    }, error=>{
        alert('Login API Fail:');
    });
  }
}
