import { Component } from '@angular/core';
import { Todos } from '../../models/todo/todos.model';
import { TodosService } from '../../services/todos.service';
import { AddTodos } from '../../models/todo/addTodo.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrl: './main.component.scss'
})
export class MainComponent {
  constructor(private todoService:TodosService,private router : Router){}
  displayCreateDialog = false;
  todosList : Todos[] = [];
  newTodo: AddTodos = new AddTodos();
  header = "";
  displayBtnSave = false; 
  editAble = false;
  displayBtnUpdate = false;
  idUpdate = "";


  ngOnInit():void{
    this.getAllTodo();
  }

  getAllTodo(){
    this.todoService.getAll().subscribe((res:Todos[])=>{
      if(res){
        this.todosList = res
      }
    });
  }

  showCreateModal() {
    this.header = "Create Todo";
    this.displayBtnSave = true;
    this.newTodo = new AddTodos();
    this.displayCreateDialog = true;
    this.editAble = false;
    this.displayBtnUpdate = false;
  }

  saveNew(){
    if(this.validate()){

    this.displayCreateDialog = false;
    this.todoService.addTodo(this.newTodo).subscribe((res)=>{
      alert('Create Success.');
      this.getAllTodo();
    },error=>{
      alert(`Create fail : ${error}`)
    })
  }
  }

  deleteTodo(id:string){
    this.todoService.deleteTodo(id).subscribe((res)=>{
      alert('Delete Success.');
      this.getAllTodo();
    },error=>{
      alert(`Delete fail : ${error}`)
    })
  }

  getTodo(id:string){
    this.todoService.getTodo(id).subscribe((res : Todos)=>{
    
    if(res){
      this.header = "Todo Detail";
      this.displayBtnSave = false;
      this.displayCreateDialog = true;
      this.editAble = true;
      this.newTodo.title = res.title;
      this.newTodo.description = res.description;
      this.displayBtnUpdate = false;
    }
    },error=>{
      alert(`Get data fail : ${error}`)
    })
    }

    getForEdit(id:string){
      this.todoService.getTodo(id).subscribe((res : Todos)=>{
      
      if(res){
        this.idUpdate = id;
        this.header = "Edit Todo";
        this.displayBtnSave = false;
        this.displayCreateDialog = true;
        this.editAble = false;
        this.newTodo.title = res.title;
        this.newTodo.description = res.description;
        this.displayBtnUpdate = true;
      }
      },error=>{
        alert(`Get data fail : ${error}`)
      })
      }

      saveUpdate(){

        if(this.validate()){

        this.todoService.updateTodo(this.idUpdate,this.newTodo).subscribe((res)=>{
          alert("Update Success");
          this.displayCreateDialog = false;
          this.getAllTodo();
        },error=>{
          alert(`Update fail : ${error}`)
        })

      }
      }

      validate(){
        if(!this.newTodo.title || !this.newTodo.description){
          alert('Input incorrect.')
          return false;
        }
        return true;
      }

      logout(){
        localStorage.clear();
        this.router.navigate(['']);
      }

  }


