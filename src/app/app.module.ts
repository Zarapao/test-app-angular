import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './view/login/login.component';
import { MainComponent } from './view/main/main.component';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { HTTP_INTERCEPTORS, HttpClientModule  } from '@angular/common/http';
import { CustomeInterceptor } from './services/custome.interceptor';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { AuthguardService } from './services/authguard.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PanelModule,
    ButtonModule,
    BrowserAnimationsModule,
    FormsModule,
    TableModule,
    HttpClientModule,
    DialogModule,
    InputTextModule
    
  ],
  providers: [
    {
      provide : HTTP_INTERCEPTORS,
      useClass : CustomeInterceptor,
      multi : true
    },AuthguardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
