import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AddTodos } from '../models/todo/addTodo.model';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private http:HttpClient) { }

  getAll(): Observable<any>{
    return this.http.get<any>(`https://candidate.neversitup.com/todo/todos`)
  }

  addTodo(data:AddTodos): Observable<any>{
    return this.http.post<any>(`https://candidate.neversitup.com/todo/todos`,data)
  }

  deleteTodo(id:string): Observable<any>{
    return this.http.delete<any>(`https://candidate.neversitup.com/todo/todos/${id}`)
  }

  getTodo(id:string): Observable<any>{
    return this.http.get<any>(`https://candidate.neversitup.com/todo/todos/${id}`)
  }

  updateTodo(id:string,data:AddTodos): Observable<any>{
    return this.http.put<any>(`https://candidate.neversitup.com/todo/todos/${id}`,data)
  }
}
