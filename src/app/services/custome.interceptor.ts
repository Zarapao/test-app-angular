import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class CustomeInterceptor implements HttpInterceptor{

  constructor(){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    
    const token = localStorage.getItem('loginToken')
    const cloneRequest = req.clone({
      setHeaders:{
        Authorization : `Bearer ${token}`
      }
    })

    return next.handle(cloneRequest);
    
  }
}


// export const customeInterceptor: HttpInterceptor = (req, next) => {
//   return next(req);
// };
