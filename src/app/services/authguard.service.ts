import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthguardService implements CanActivate {
  constructor(private router:Router){}
  canActivate(): boolean  {
    if(localStorage.getItem("loginToken")){
      return true;
    }
    this.router.navigate(['']);
    return false;
  }
  
}
