import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }

  apiLogin(userName:string,password:string): Observable<any>{
    let payload = {
      username: userName,
      password:password
    }
    return this.http.post<any>(`https://candidate.neversitup.com/todo/users/auth`,payload);
  }



}

