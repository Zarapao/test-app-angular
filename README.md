# 1 Project Structure

```
📦 Project
┣ 📂 src
┃ ┣ 📂 app
┃ ┃ ┣ 📂 models
┃ ┃ ┣ 📂 services
┃ ┃ ┃ ┣ 📃 authGuard.service.ts
┃ ┃ ┃ ┗ 📃 custom.interceptor.ts
┃ ┃ ┣ 📂 view
┃ ┃ ┃ ┗ 📂 master
┃ ┃ ┣ 📃 app-routing.module.ts
┃ ┃ ┗ 📃 app.module.ts
┃ ┣ 📂 assets
┃ ┣ 📂 environment
┃ ┣ 📃 index.html
┃ ┣ 📃 main.js
┃ ┗📃 styles.scss
┣ 📃 angular.json
┣ 📃 package.json


```

# 2 Class file naming
Class names are written in [CamelCase].
Example: `masterData`, `masterModel`.

